#!/bin/sh

#inFile="/hepgpu1-data3/jrawling/data16_13TeV/data16_13TeV_25fb.root"
#inFile="/data/jlue/EtaCal/EtaInterCal/AF/user.jlue.mc16lowmu_13TeV.SherpaString.DAOD_JETM1.p4396.20221202.2233_EtaInterCal_3DHistos.root/user.jlue.31468990._000001.EtaInterCal_3DHistos.root"
#inFile="/data/jlue/EtaCal/EtaInterCal/AF/mc/mc16lowmu_13TeV.SherpaString.DAOD_JETM1.p4396.20221202.2233_EtaInterCal_3DHistos.root"

#inFile="/data/jlue/EtaCal/EtaInterCal/AF/data18_13TeV.DAOD_JETM1.p5462.20221205.0511_EtaInterCal_3DHistos.root"
#inFile="~/LocalOutput/data18_13TeV.00354396.physics_MinBias.DAOD_JETM1.31388768.EtaInterCal_3DHistos.root"
#inFILE="/home/jlue/LocalOutput/data18_13TeV.00354396.physics_MinBias.DAOD_JETM1.31388768.EtaInterCal_3DHistos.root"
#inFile="/data/jlue/EtaCal/EtaInterCal/AF/mc/mc16lowmu_Pythia8_Jan2023_p4396.root"
#inFile="/data/jlue/EtaCal//EtaInterCal/AF/user.jlue.data18_13TeV.MinBias.p5462.20230303.0557_EtaInterCal_3DHistos.root"
#inFile="/data/jlue/data/MinBias.data18_13TeV.DAOD_JETM1.p5462.20230309.2324_EtaInterCal_3DHistos.root"
#inFile="~/LocalOutput/data18_13TeV.00354396.physics_MinBias.DAOD_JETM1.31388768.EtaInterCal_3DHistos.root"
#inFile="/data/jlue/EtaCal/EtaInterCal/AF/MINBIASFORTRIGSTUDIES/user.jlue.data18_13TeV.p5462.20230318.EtaInterCal_3DHistos.root "
inFile="/data/jlue/EJ/Py8EG_Zprime2EJsDAODs/test.root"

#configFile="utils/lowmusettings.config"
#outPath="~/LocalOutput/TriggerEfficiency/MinBias20230319"
#configFile="utils/lowmusettings20230411.config"
#outPath="/data/jlue/EtaCal/TriggerEfficiencies/MinBias"
configFile="utils/lowmusettingsdetail.config"
outPath="/data/jlue/delme"
exec=triggerEfficiency.exe
./$exec ${configFile} ${inFile} ${outPath}
#&> ${outPath}/generation.log &
