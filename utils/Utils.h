/*
 * Author: Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>
 */
 #ifndef UTIL_H
 #define UTIL_H

// C++ includes
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <time.h>

//#include "EventShapeTools/EventShapeCopier.h"

// ROOT includes
#include "TSystem.h"
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TEnv.h"
#include "TError.h"
#include "TChain.h"
#include "TTree.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile2D.h"
#include "TLorentzVector.h"

//static float refRegion = 0.8;
//static float centralRegion = 2.4;

using namespace std;

typedef vector<TString> StrV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector<float> VecF;
typedef vector<int> VecI;

void error(Str msg);

TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh);
TH1F* createHist1D(TString hname, TString title, std::vector<double> bins);
TH2F* createHist2D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh);
TH2F* createHist2D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins);
TH3F* createHist3D(TString hname, TString title, int xnbins, float xlow, float xhigh, int ynbins, float ylow, float yhigh, int znbins, float zlow, float zhigh) ;
TH3F* createHist3D(TString hname, TString title, std::vector<double> xbins, std::vector<double> ybins, std::vector<double> zbins);
TH1F *GetTH1F(TFile *f, Str hn);
TH2F *GetTH2F(TFile *f, Str hn);
std::vector<TString> vectorise(TString str, TString sep=" ") ;
std::vector<double> vectoriseD(TString str, TString sep=" ");
std::vector<int> vectoriseI(TString str, TString sep=" ");
std::vector<double> makeUniformVec(int N, double min, double max) ;

void PrintTime();
TEnv *OpenSettingsFile(Str fileName);
TFile *InitOutputFile(Str ofName);

#endif
