#!/bin/sh

includes=""
pkgs=""

echo "Includes: $includes"
#mylibs=`$ROOTCOREDIR/scripts/get_ldflags.sh $pkgs`
mylibs=""
echo "Libraries: $mylibs"

flagsNlibs="`$ROOTSYS/bin/root-config --cflags --glibs` -lTreePlayer -lHistPainter -lHist -lMatrix -lRIO -lTreePlayer -lMinuit -lMathCore -lMathMore  -lXMLParser $mylibs"
code="utils/Utils.C TriggerEfficencyEvaluator.C main.C utils/AtlasStyle.C "
exec=triggerEfficiency.exe
rm -f $exec

g++ $flagsNlibs $includes -o $exec $code
